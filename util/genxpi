#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#
# This script generates the Enigmail XPI
#

echo "genxpi: Generating $1 in $3"

if [ $# -lt 5 ]; then
  echo "Wrong number of parameters"
  exit 1
fi

xpiFile=$1
targetTool="$2"
distDir="$3"
srcDir=$4
xpiModule=$5
enableLang=$6

cd ${srcDir}
cwd=`pwd`
cd "$distDir"
targetDir=`pwd`
cd "$cwd"

# Prepare chrome.manifest
cat  ${srcDir}/package/chrome.manifest \
> ${targetDir}/chrome.manifest

cat  ${srcDir}/package/schema.json \
> ${targetDir}/schema.json

# Prepare languages other than en-US

if [ "$enableLang" = "yes" ]; then
  if [ -s ${srcDir}/lang/current-languages.txt ]; then
    echo '' >> ${targetDir}/chrome.manifest
    echo '# Additional languages' >> ${targetDir}/chrome.manifest

    for lang in `cat ${srcDir}/lang/current-languages.txt`; do
      echo 'locale        enigmail    '$lang'       chrome/locale/'$lang'/' >> ${targetDir}/chrome.manifest
    done
  fi
fi


cd "$targetDir"

find chrome/content/modules -name "*.js*" | LC_ALL=C sort > chrome/content/modules/all-modules.txt

echo "Creating ${xpiFile} file"

zip -9 --must-match\
    ../${xpiFile} \
    chrome/content/preferences/defaultPrefs.js \
    chrome/content/modules/app.jsm \
    chrome/content/modules/armor.jsm \
    chrome/content/modules/buildDate.jsm \
    chrome/content/modules/clipboard.jsm \
    chrome/content/modules/constants.jsm \
    chrome/content/modules/cryptoAPI.jsm \
    chrome/content/modules/cryptoAPI/gnupg.js \
    chrome/content/modules/cryptoAPI/gnupg-agent.jsm \
    chrome/content/modules/cryptoAPI/gnupg-core.jsm \
    chrome/content/modules/cryptoAPI/gnupg-key.jsm \
    chrome/content/modules/cryptoAPI/gnupg-keylist.jsm \
    chrome/content/modules/cryptoAPI/interface.js \
    chrome/content/modules/data.jsm \
    chrome/content/modules/dialog.jsm \
    chrome/content/modules/core.jsm \
    chrome/content/modules/enigmailOverlays.jsm \
    chrome/content/modules/errorHandling.jsm \
    chrome/content/modules/funcs.jsm \
    chrome/content/modules/execution.jsm \
    chrome/content/modules/files.jsm \
    chrome/content/modules/key.jsm \
    chrome/content/modules/keyObj.jsm \
    chrome/content/modules/keyRing.jsm \
    chrome/content/modules/keyUsability.jsm \
    chrome/content/modules/lazy.jsm \
    chrome/content/modules/locale.jsm \
    chrome/content/modules/localizeHtml.jsm \
    chrome/content/modules/log.jsm \
    chrome/content/modules/os.jsm \
    chrome/content/modules/overlays.jsm \
    chrome/content/modules/passwords.jsm \
    chrome/content/modules/pipeConsole.jsm \
    chrome/content/modules/prefs.jsm \
    chrome/content/modules/searchCallback.jsm \
    chrome/content/modules/singletons.jsm \
    chrome/content/modules/streams.jsm \
    chrome/content/modules/subprocess.jsm \
    chrome/content/modules/enigmailprocess_shared_unix.js \
    chrome/content/modules/enigmailprocess_worker_common.js \
    chrome/content/modules/enigmailprocess_common.jsm \
    chrome/content/modules/enigmailprocess_shared_win.js \
    chrome/content/modules/enigmailprocess_worker_unix.js \
    chrome/content/modules/enigmailprocess_main.jsm \
    chrome/content/modules/enigmailprocess_unix.jsm \
    chrome/content/modules/enigmailprocess_worker_win.js \
    chrome/content/modules/enigmailprocess_shared.js \
    chrome/content/modules/enigmailprocess_win.jsm \
    chrome/content/modules/system.jsm \
    chrome/content/modules/compat.jsm \
    chrome/content/modules/time.jsm \
    chrome/content/modules/timer.jsm \
    chrome/content/modules/trust.jsm \
    chrome/content/modules/versioning.jsm \
    chrome/content/modules/windows.jsm \
    chrome/content/modules/all-modules.txt

if [ $? -ne 0 ]; then
  exit 1
fi

zip -9 \
    ../${xpiFile} \
    chrome/content/ui/*.* \
    chrome/content/skin/*.* \
    chrome/locale/*/*.* \
    webextension.js \
    chrome.manifest \
    schema.json \
    manifest.json
